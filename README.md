Rouge Beauty Bar offers a relaxing environment for you to get ready for your next big event or for a little self love time. We can do your hair, makeup, brows, lashes all in one place while you relax with a complimentary mimosa or tea! We also specialize in natural hair & skin care.

Address: 2825 S Glenstone Ave, Suite P03A, Springfield, MO 65804, USA

Phone: 417-350-1372

Website: http://www.rougebeautybar417.com
